(function(window) {
  window.recursiveSearch = treeSearch;

  // Рекурсивный вызов, это плохой вариант для больших деревьев,
  // callstack не резиновый
  function treeSearch(tree, keyword) {
    // В результате такого поиска, мы мутируем существующее дерево.
    // Чтобы этого избежать, можно в этом месте сделать глубокую копию, но это
    // потери по времени.
    // Если хотим и дерево пройти и копию сделать, нужно вместо filter написать
    // reduce, который вернет массив, а в searchByChildren возвращать новый
    // объект.
    // Таким образом получим новое дерево за один проход, вместе с поиском.
    searchByChildren(tree, keyword);

    return tree;
  }

  function searchByChildren(currentNode, keyword) {
    var children = currentNode.children || [];

    var matchedChildren = children.filter(function(child) {
      return searchByChildren(child, keyword);
    });

    currentNode.children = matchedChildren;

    if (isContainKeyword(currentNode, keyword)) currentNode.matched = true;

    if (currentNode.children.length) currentNode.expanded = true;

    return currentNode.matched || currentNode.children.length;
  }

  function isContainKeyword(node, keyword) {
    return (node.name || '').includes(keyword);
  }
})(window);
