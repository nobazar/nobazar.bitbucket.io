// var tree = {};
var tree = require('./test_data');

search(tree);

function search(tree, keyword) {
  var treeByLevels = splitTreeByLevels(tree);

  var filteredVertices = treeByLevels.map(function(vertices) {
    var matches = vertices.filter(function(v) {
      return v.name.includes(keyword);
    });

    return matches;
  });

  console.log(filteredVertices);
}

function getVertex(obj, path) {
  var level = path.split('.').length - 1;
  var lastIndex = path.split('.').pop();

  return obj[level][lastIndex];
}

function splitTreeByLevels(tree) {
  var rootVertex = copyVertex(tree);

  rootVertex.key = '0';

  var verticesByLevel = [[rootVertex]];

  while (true) {
    var lastLevel = verticesByLevel[verticesByLevel.length - 1];

    if (!lastLevel.length) break;

    verticesByLevel.push(getLevelVertices(lastLevel));
  }

  return verticesByLevel;
}

function getLevelVertices(vertices) {
  var flatChilds = [];

  vertices.forEach(function(vertex) {
    var vertexChildren = getChildrenCopy(vertex);

    flatChilds.push.apply(flatChilds, vertexChildren);
  });

  return flatChilds;
}

function getChildrenCopy(vertex) {
  var children = vertex.children;

  return children.map(function(child, index) {
    var copy = copyVertex(child);

    copy.key = vertex.key + '.' + index;

    return copy;
  });
}

function copyVertex(vertex) {
  return {
    name: vertex.name,
    expanded: vertex.expanded,
    matched: vertex.matched,
    children: vertex.children
  };
}
