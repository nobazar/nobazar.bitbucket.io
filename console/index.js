const tree = require('./test_data');

const searchStr = process.argv[2];
const resultTree = treeSearch(tree, searchStr);

// Рекурсивный вызов, это плохой вариант для больших деревьев,
// callstack не резиновый
consoleTree(resultTree);

function consoleTree(tree, offset = 0) {
  const { name } = tree;
  const prefix = tree.children.length ? '- ' : '  ';
  const displayNode = tree.matched ? `[${name}]` : tree.name;

  console.log(' '.repeat(offset) + prefix + displayNode);

  tree.children.forEach(node => consoleTree(node, offset + 2));
}

function treeSearch(tree, keyword) {
  // В результате такого поиска, мы мутируем существующее дерево.
  // Чтобы этого избежать, можно в этом месте сделать глубокую копию, но это
  // потери по времени.
  // Если хотим и дерево пройти и копию сделать, нужно вместо filter написать
  // reduce, который вернет массив, а в searchByChildren возвращать новый
  // объект.
  // Таким образом получим новое дерево за один проход, вместе с поиском.
  searchByChildren(tree, keyword);

  return tree;
}

function searchByChildren(currentNode, keyword) {
  const { children } = currentNode;

  const matchedChildren = children.filter(child =>
    searchByChildren(child, keyword)
  );

  currentNode.children = matchedChildren;

  if (isContainKeyword(currentNode, keyword)) currentNode.matched = true;

  if (currentNode.children.length) currentNode.expanded = true;

  return currentNode.matched || currentNode.children.length;
}

function isContainKeyword(node, keyword) {
  return node.name.includes(keyword);
}
